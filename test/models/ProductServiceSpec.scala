package models

import dal.ProductDal
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play.PlaySpec

import scala.concurrent.Future

class ProductServiceSpec extends PlaySpec with MockFactory {

  val dal = mock[ProductDal]
  val prodSvcSpec = new ProductServiceImpl(dal)

  "ProductService" must {
    "fetch all products " in {
      import scala.concurrent.ExecutionContext.Implicits._
      val ms = Seq("b", "a")
      val mF = Future {
        ms
      }
      (dal.findAllProducts _).expects().returning(mF)
      prodSvcSpec.findAllProductNames mustBe ms
    }
  }
}

