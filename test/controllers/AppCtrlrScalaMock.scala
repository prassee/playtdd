package controllers

import com.google.inject.Inject
import dal.ProductDal
import models.{ProductService, ProductServiceImpl, UserService}
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play.PlaySpec
import play.api.mvc.Results
import play.api.test.FakeRequest
import play.api.test.Helpers.{contentAsString, defaultAwaitTimeout}

import scala.concurrent.Future

/**
 *
 */
class ApplicationControllerSpec extends PlaySpec with Results with MockFactory {
  class TestApplicationController @Inject() (userSvc: UserService, productService: ProductService)
    extends ApplicationController(userSvc, productService)
  "Application Controller" must {
    val mockUsrSvc = mock[UserService]
    val mockProdSvc = mock[ProductService]
    val ctrl = new TestApplicationController(mockUsrSvc, mockProdSvc)

    "give response" in {
      (mockUsrSvc.getUser _).expects().returning("value")
      val resp = ctrl.authUser.apply(FakeRequest())
      contentAsString(resp) must include("value")
    }

    "action calling service calling db" in {

      (mockProdSvc.findAllProductNames _).expects().returning(Seq())
      ctrl.getProducts.apply(FakeRequest())
    }
  }

  "Product service with injected mock DAL" must {
    import scala.concurrent.ExecutionContext.Implicits._
    val dal = mock[ProductDal]
    val prodSvcImpl = new ProductServiceImpl(dal)
    "return the expected seq" in {
      val ms = Seq("b", "a")
      val mF = Future {
        ms
      }
      (dal.findAllProducts _).expects().returning(mF)
      prodSvcImpl.findAllProductNames mustBe ms
    }
  }

}
