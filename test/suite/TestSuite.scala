package suite

import org.scalatest.Suites

import controllers.{ApplicationControllerSpec, AppCtrlrScalaMock}

class TestSuite extends Suites(new ApplicationControllerSpec())
