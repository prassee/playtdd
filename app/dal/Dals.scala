package dal

import models.Products
import slick.driver.H2Driver.api._
import slick.lifted.TableQuery
import scala.concurrent.Future
import com.google.inject.ImplementedBy

object ProductDal extends ProductDal {

  private val products = TableQuery[Products]
  private val db = Database.forConfig("h2mem1")

  def findAllProducts(): Future[Seq[String]] = {
    val x = for (prod <- products) yield " " + prod.id + " " + prod.cost
    val fut = db.run(x.result)
    fut
  }

}
trait ProductDal {
  def findAllProducts(): Future[Seq[String]]
}
