package controllers

import com.google.inject.Inject

import models.{ ProductService, UserService }
import play.api.mvc.{ Action, Controller }

import com.google.inject.{ ImplementedBy, Inject }

import models.{ ProductService, UserService }
import play.api.mvc.{ Action, Controller }

class ApplicationController @Inject() (usrSvc: UserService, productService: ProductService) extends Controller {

  def authUser = {
    Action(Ok(usrSvc.getUser))
  }

  def getUserInfo(a: String) = {
    val user = a + " " + usrSvc.getUser
    Action(Ok(user))
  }

  def getProducts = {
    Action(Ok(productService.findAllProductNames.mkString(" ")))
  }
}
