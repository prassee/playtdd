package models

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Failure, Success }
import com.google.inject.ImplementedBy
import dal.ProductDal
import javax.inject.Inject
import scala.concurrent.Await
import java.util.concurrent.TimeUnit

@ImplementedBy(classOf[UserServiceImpl])
trait UserService {
  def getUser: String

  def getUserInfo(a: String): String
}

class UserServiceImpl extends UserService {
  override def getUser: String = "user service called"
  override def getUserInfo(a: String): String = s"this is user info of $a"
}

@ImplementedBy(classOf[ProductServiceImpl])
trait ProductService {
  def findAllProductNames: Seq[String]
}

class ProductServiceImpl(prodDal: ProductDal) extends ProductService {

  override def findAllProductNames: Seq[String] = {
    var result = Seq[String]()
    val fut = prodDal.findAllProducts()
    result = Await.result(fut, scala.concurrent.duration.Duration(2, TimeUnit.SECONDS))
    result
  }
}
