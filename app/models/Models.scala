package models


import slick.driver.H2Driver.api._

case class Product(id: String, cost: Double, curr: String)

class Products(tag: Tag) extends Table[Product](tag, "Products") {

  def id = column[String]("id", O.PrimaryKey, O.AutoInc)

  def cost = column[Double]("cost")

  def curr = column[String]("curr")

  def * = (id, cost, curr) <> (Product.tupled, Product.unapply)
}


