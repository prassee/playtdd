name := "playtdd"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.11.6"

lazy val playtdd = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "1.1.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "1.1.0",
  "com.h2database" % "h2" % "1.4.177",
  "org.scalatestplus" % "play_2.11" % "1.2.0" % "test",
  "org.mockito" % "mockito-all" % "1.10.19",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test"
)

